#include "cerc.h"
#include <graphics.h>
#include <string.h>

Cerc::Cerc():Figura(){
	
	r=50;
	c=YELLOW;
	strcpy(this->Nume, "Ana");
}

Cerc::Cerc(int stdr, int sj, int raza, int color, char Nume[] ):Figura(stdr,sj){
	
	r=raza;
	c=color;
	strcpy(this->Nume,Nume);
	
};

void Cerc::Muta(int dx,int dy){
	Sterge(); 

	x += dx;         
	y += dy;

	Afiseaza();  
}

void Cerc::Creste(int dr){
	Sterge(); 
	r+=dr;
	Afiseaza(); 
}
void Cerc::Sterge (){
	setcolor(BLACK);  
    circle(x,y,r);
	settextjustify(CENTER_TEXT,CENTER_TEXT);
	outtextxy(x,y,Nume);
}

void Cerc::Afiseaza (){
	setcolor(c);
    circle(x,y,r);
	settextjustify(CENTER_TEXT,CENTER_TEXT);
	outtextxy(x,y,Nume);
}
