#include <graphics.h>
#include "Figura.h"
class Cerc: public Figura{
public:
	void Creste(int);
	void Muta(int,int);
	void Afiseaza();
	Cerc(int x, int y, int r=20, int c=WHITE, char Nume[]="Ion");
	Cerc();
	void Insereaza();
	void Elimina();
	void Sterge ();
private:
	int r;
	char Nume[10];
};

